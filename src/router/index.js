import Vue from 'vue'
import Router from 'vue-router'
import TodoApp from '@/components/TodoApp'
import Clock from '@/components/clock'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/todo',
      name: 'TodoApp',
      component: TodoApp
    },
    {
      path: '/clock/',
      name: 'Clock',
      component: Clock
    }
  ]
})
