// initial state
const state = {
    todos: [
        {
            id: 1,
            title: "Complete ten push-ups",
            dueDate: "Fri Aug 8",
            status: "due"
        },
        {
            id: 2,
            title: "Interview Scheduled for mike",
            dueDate: "Sun Aug 10",
            status: "due"
        },
        {
            id: 3,
            title: "Meeting",
            dueDate: "Sun Aug 10",
            status: "due"
        },
        {
            id: 4,
            title: "Party at Jason's house",
            dueDate: "Fri Aug 8",
            status: "done"
        }
    ]
}

// getters
const getters = {
    allTasks: (state, getters, rootState) => {
        return rootState.todos.todos
    }
}

// actions
const actions = {
    pushTaskToList({ commit }, task) {
        commit('pushTaskToList', task)
    },
    deleteTaskFromList({ commit }, taskId) {
        commit('deleteTaskFromList', taskId)
    }

}

// mutations
const mutations = {
    pushTaskToList(state, task) {
        state.todos.push(task)
    },

    deleteTaskFromList(state, taskId) {
        state.todos.splice(state.todos.findIndex((task) => taskId == task.id), 1)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}